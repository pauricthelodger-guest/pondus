pondus (0.8.0-3) unstable; urgency=medium

  * Team upload.
  * Standards-Version: 4.1.3
  * Take over package into Debian Med Git
  * Remove debian/menu since there is a desktop file

 -- Andreas Tille <tille@debian.org>  Mon, 12 Feb 2018 16:14:54 +0100

pondus (0.8.0-2) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.
  [ Andreas Tille ]
  * debian/control:
     - cme fix dpkg-control
     - debhelper 9
  * debian/copyright: DEP5
  [ Eike Nicklas ]
  * debian/control:
     - set Python Applications Packaging Team as Maintainer and myself as
       Uploader

 -- Eike Nicklas <eike@ephys.de>  Mon, 16 Dec 2013 22:08:54 +0100

pondus (0.8.0-1) unstable; urgency=low

  * new upstream release
  * update Standards-Version to 3.9.2: no changes required
  * switch to debhelper 7
  * debian/control:
    - update Description
    - remove ${python:Breaks}

 -- Eike Nicklas <eike@ephys.de>  Thu, 09 Jun 2011 10:42:07 +0100

pondus (0.7.3-1) unstable; urgency=low

  * switch from python-support to dh_python2
  * update Standards-Version to 3.9.1: no changes required
  * debian/control:
    - update homepage
  * debian/copyright:
    - update for 2010 and 2011
    - update to conform with DEP5 candidate
  * debian/rules:
    - add '--install-layout=deb' and remove '--prefix=/usr'
  * debian/source/format:
    - change source format to 3.0 (quilt)
  * debian/watch:
    - update homepage

 -- Eike Nicklas <eike@ephys.de>  Fri, 21 Dec 2010 21:30:38 +0100

pondus (0.7.2-1) unstable; urgency=low

  * new upstream release
  * update Standards-Version to 3.8.4: no changes required
  * debian/control:
    - python2.4 is not available any more: adjust Depends and
      XS-Python-Version

 -- Eike Nicklas <eike@ephys.de>  Sun, 14 Feb 2010 17:59:41 +0100

pondus (0.7.1-1) unstable; urgency=low

  * new upstream release
  * debian/control:
    - add ${misc:Depends} to Depends to make lintian happy
  * debian/watch:
    - fix for new upstream homepage structure

 -- Eike Nicklas <eike@ephys.de>  Mon, 18 Jan 2010 11:06:53 +0100

pondus (0.7.0-1) unstable; urgency=low

  * new upstream release
  * debian/control:
    - bump python-gtk2 dependency to >=2.12 (new upstream requirement)
  * debian/copyright:
    - conform to DEP-5
    - change license of upstream code from GPL-3+ to Expat
    - change license of Debian packaging from GPL-3+ to Expat

 -- Eike Nicklas <eike@ephys.de>  Mon, 21 Dec 2009 23:20:50 +0100

pondus (0.6.0-2) unstable; urgency=low

  * update Standards-Version to 3.8.3: no changes required
  * fix installation with python2.6 (Closes: #557918)

 -- Eike Nicklas <eike@ephys.de>  Wed, 25 Nov 2009 23:25:25 +0100

pondus (0.6.0-1) unstable; urgency=low

  * new upstream release
  * update Standards-Version to 3.8.2: no changes required

 -- Eike Nicklas <eike@ephys.de>  Sun, 19 Jul 2009 12:53:58 +0100

pondus (0.5.3-1) unstable; urgency=low

  * new upstream bugfix release
  * debian/rules:
    - don't make all .py files executable, since shebang was removed

 -- Eike Nicklas <eike@ephys.de>  Wed, 10 Dec 2008 22:48:56 +0100

pondus (0.5.2-1) unstable; urgency=low

  [ Eike Nicklas]
  * new upstream release

  [ Marco Rodrigues ]
  * debian/watch:
    + Fix URL to work correctly.

  [ Sandro Tosi ]
  * debian/control
    - switch Vcs-Browser field to viewsvn

 -- Eike Nicklas <eike@ephys.de>  Wed, 07 Dec 2008 10:33:52 +0100

pondus (0.5.1-1) unstable; urgency=low

  * new upstream bugfix release

 -- Eike Nicklas <eike@ephys.de>  Fri, 05 Sep 2008 09:57:09 +0100

pondus (0.5.0-1) UNRELEASED; urgency=low

  * new upstream release
  * update Standards-Version to 3.8.0: no changes required
  * debian/control:
     - depend on python-elementree (new upstream requirement)

 -- Eike Nicklas <eike@ephys.de>  Mon, 25 Aug 2008 09:17:14 +0100

pondus (0.4.1-1) unstable; urgency=low

  * new upstream bugfix release

 -- Eike Nicklas <eike@ephys.de>  Fri, 16 May 2008 01:12:20 +0100

pondus (0.4.0-1) unstable; urgency=low

  * new upstream release
  * debian/control: update Description

 -- Eike Nicklas <eike@ephys.de>  Sat, 12 Apr 2008 10:32:13 +0100

pondus (0.3.0-1) unstable; urgency=low

  * new upstream release
  * debian/control:
    - remove gettext from Build-Depends-Indep; *.mo now distributed
      with upstream
    - mention plotting capability in Description
    - move python-matplotlib from Depends to Recommends as it is not
      strictly required any more
    - depend on python-gtk2 >= 2.6 (updated upstream requirement)
  * debian/copyright:
    - use proposed machine-readable format

 -- Eike Nicklas <eike@ephys.de>  Thu, 06 Mar 2008 20:25:48 +0100

pondus (0.2.0-1) unstable; urgency=low

  * new upstream release
  * debian/control:
    - require debhelper >= 5.0.51 since dh_icons is used in debian/rules
    - add Debian-Med Packaging Team to uploaders
    - add gettext to Build-Depends-Indep

 -- Eike Nicklas <eike@ephys.de>  Mon, 18 Feb 2008 21:54:24 +0100

pondus (0.1.0-1) unstable; urgency=low

  * Initial release (Closes: #463873)

 -- Eike Nicklas <eike@ephys.de>  Fri, 01 Feb 2008 20:15:52 +0100
