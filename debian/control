Source: pondus
Maintainer: Debian-Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Eike Nicklas <eike@ephys.de>
Section: x11
Priority: optional
Build-Depends: debhelper (>= 9),
               python
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/med-team/pondus.git
Vcs-Git: https://salsa.debian.org/med-team/pondus.git
Homepage: http://bitbucket.org/eike/pondus/
X-Python-Version: >= 2.5

Package: pondus
Architecture: all
Depends: ${python:Depends},
         ${misc:Depends},
         python-gobject,
         python-gtk2
Recommends: python-matplotlib
Description: personal weight manager for GTK+2
 Pondus keeps track of your body weight and, optionally, the percentage of
 bodyfat, muscle and water. It aims to be simple to use, lightweight and
 fast. All data can be plotted to get a quick overview of the history of
 your weight. A simple weight planner allows one to define "target weights"
 and this plan can be compared with the actual measurements in a plot.
