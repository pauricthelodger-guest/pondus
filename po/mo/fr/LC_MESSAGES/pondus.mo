��    N      �  k   �      �     �     �     �  #   �  �   �     �  
   �     �     �  
   �     �     �     �     �               /  *   G     r          �     �     �     �     �     �     	     	     *	  
   8	  	   C	     M	  3   g	  $   �	     �	     �	     �	     �	     �	     �	     	
     
  	   +
     5
     H
     U
     g
     z
     �
  
   �
  .   �
     �
  .   �
       -   ,  <   Z  J   �     �  S   �     Q  8   ^     �     �     �     �  #   �     �     �     �                     
       6     
   J     U  �  e     �             /   '  �   W          2  #   B  !   f     �     �     �     �  0   �            !     ?   ?          �  !   �     �     �               0     M     e     z     �     �     �  K   �  +     	   :     D     T     \     |     �  #   �     �     �     �           "     C     T     e     m  2   |     �  0   �     �  ;   
  D   F  M   �  +   �  r        x  C   �     �     �     �       '        C     F  	   L     V     ]     `     g  	   i  +   s  	   �     �     =      >   A       "   J   ?              .   I           C   %                   D      5   F   -       L               	      6   <   3      2           4   !                  +                  $   &   #   K   9      (   )   1         N               /                         M      G           ,       @      0                 :          B       '      ;   7   
         *   H   8   E        Add Dataset Add dataset All Time An error occured during the import. Another instance of pondus seems to be editing the same datafile. Do you really want to continue and loose all the changes from the other instance? Body Mass Index CSV Export CSV File to read from: CSV File to save to: CSV Import Could not find %s Custom Data to export: Datafile locked, continue? Date Date (YYYY-MM-DD) Delete selected dataset Do you really want to delete this dataset? Edit Dataset Edit selected dataset Enable Weight Planner End date Error: Not a valid File Error: Wrong Format Export successful Import data to: Import not successful Import successful Last 6 Months Last Month Last Year Matplotlib not available! Not owning the file lock. Backing up the data to %s Please make sure pygtk is installed. Plot Plot Weight Preferences Preferred Unit System: Quit Reading file %s Remember Window Size Remove Data? Save Plot Save as File Type: Save to File Saving plot to %s Select Date Range: Select File Smooth Start date The data entered is not in the correct format! The export was successful. The given path does not point to a valid file! The import was successful. The start date has to be before the end date! The weight planner can be enabled in the preferences dialog. To plot your BMI, you need to enter your height in the preferences dialog. Use Calendar in Add Dialog Use a calendar widget instead of a text entry to enter dates in the add/edit dialog User Height: Using the standard file ~/.pondus/user_data.xml instead. Weight Weight Measurements Weight Plan Weight Planner You passed a directory, not a file! cm ft imperial in kg lbs m metric python-matplotlib is not installed, plotting disabled! weight.csv weight_plot.png Project-Id-Version: pondus
Report-Msgid-Bugs-To: http://bitbucket.org/eike/pondus/issues
POT-Creation-Date: 2011-05-28 20:39+0200
PO-Revision-Date: 2011-05-26 17:41+0000
Last-Translator: eike <eike@ephys.de>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=2; plural=(n > 1)
 Nouvel enregistrement Nouvel enregistrement Toutes les dates Une erreur s'est produite lors de l'importation Une autre instance de pondus semble être en train d'éditer le même fichier de données. Voulez-vous vraiment continuer et perdre tous les changements effectués par l'autre instance ? Index de Masse Corporelle (IMC) Exportation CSV Chemin du fichier CSV à importer : Fichier CSV à enregistrer sous : Importation CSV Fichier introuvable: %s Personnalisé Données à exporter : Fichier de données déjà utilisé, continuer ? Date Date (AAAA-MM-JJ) Supprimer la ligne sélectionnée Êtes vous certain(e) de vouloir supprimer cet enregistrement ? Modifier l'enregistrement Éditer la ligne sélectionnée Activer le planificateur de poids Date de fin Erreur : fichier invalide Erreur : format incorrect Exportation réussie Importer les données vers : Échec de l'importation Importation réussie 6 derniers mois Mois dernier Année dernière Matplotlib indisponible ! Ne possède pas le fichier de verrouillage. Sauvegarde des données vers %s Veuillez vérifier que pygtk est installé. Diagramme Courbe de poids Options Système d'unités préféré : Quitter Lecture du fichier %s Mémoriser la taille de la fenêtre Supprimer l'enregistrement ? Enregistrer le diagramme Type de fichier : Enregistrer dans un fichier Enregistrer le diagramme sous %s Plage de dates : Choix du fichier Finesse Date de début La saisie n'a pas été effectuée au bon format ! Exportation réussie Le chemin indiqué n'est pas un fichier valide ! Importation réussie La date de départ doit être antérieure à celle de fin ! Le planificateur de poids peut être activé dans les préférences. Pour tracer votre IMC, vous devez saisir votre taille dans les préférences. Utiliser le calendrier dans le menu d'ajout Utiliser un widget calendrier plutôt qu'une entrée texte pour saisir les dates dans la fenêtre d'ajout/édition Taille de l'utilisateur : Utilisation du fichier standard ~/.pondus/user_data.xml à la place Poids Mesures de poids Planificateur de poids Planificateur de poids Ceci est un répertoire, pas un fichier cm pieds impérial pouces kg livres m métrique Matplotlib indisponible, tracé désactivé poids.csv courbe_de_poids.png 