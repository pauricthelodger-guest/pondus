��    P      �  k         �     �     �     �  #   �  �        �  
   �     �     �  
   �     �                    8     =     O  *   g     �     �     �     �     �     �      	     	     "	     8	     J	  
   X	  	   c	     m	  3   �	  $   �	     �	     �	     �	     
     
     %
     *
     :
     O
  	   \
     f
     y
     �
     �
     �
     �
     �
  
   �
  .   �
       .   -     \  -   w  <   �  J   �     -  S   H     �  8   �     �     �     �     	  #        <     ?     B     K     N     Q     U     W  6   ^  
   �     �  �  �     _     t     �  2   �  �   �     i     �     �     �     �     �     �     �  .   �     ,     1     C  3   _     �  #   �     �     �     �       "   (     K  $   ^  "   �     �     �     �     �  E   �  $   1     V     f       
   �     �     �     �  &   �                #     =     N      l     �      �     �     �  .   �  (     6   1  (   h  C   �  =   �  d     5   x  <   �     �  E         F     K     `     t  %   �     �     �     �     �     �     �     �     �  F   �          %     ?      @   C       "   L   A              /   K           E   %                   F      7   H   .       N               	      8   >   5      4   M       6   !                  ,                  $   '   #   :   ;      )   *   2         P               0                 &       O      I           -       B      1                 <          D       (      =   9   
         +   J   3   G        Add Dataset Add dataset All Time An error occured during the import. Another instance of pondus seems to be editing the same datafile. Do you really want to continue and loose all the changes from the other instance? Body Mass Index CSV Export CSV File to read from: CSV File to save to: CSV Import Could not find %s Custom Data to export: Datafile locked, continue? Date Date (YYYY-MM-DD) Delete selected dataset Do you really want to delete this dataset? Edit Dataset Edit selected dataset Enable Weight Planner End date Error: Not a valid File Error: Wrong Format Export successful Import data to: Import not successful Import successful Last 6 Months Last Month Last Year Matplotlib not available! Not owning the file lock. Backing up the data to %s Please make sure pygtk is installed. Plot Plot Weight Plot weight data Preferences Preferred Unit System: Quit Reading file %s Remember Window Size Remove Data? Save Plot Save as File Type: Save to File Saving plot to %s Select Date Range: Select File Select date range of plot Smooth Start date The data entered is not in the correct format! The export was successful. The given path does not point to a valid file! The import was successful. The start date has to be before the end date! The weight planner can be enabled in the preferences dialog. To plot your BMI, you need to enter your height in the preferences dialog. Use Calendar in Add Dialog Use a calendar widget instead of a text entry to enter dates in the add/edit dialog User Height: Using the standard file ~/.pondus/user_data.xml instead. Weight Weight Measurements Weight Plan Weight Planner You passed a directory, not a file! cm ft imperial in kg lbs m metric python-matplotlib is not installed, plotting disabled! weight.csv weight_plot.png Project-Id-Version: pondus
Report-Msgid-Bugs-To: http://bitbucket.org/eike/pondus/issues
POT-Creation-Date: 2011-05-28 20:39+0200
PO-Revision-Date: 2011-05-28 18:42+0000
Last-Translator: eike <eike@ephys.de>
Language-Team: Italian (http://www.transifex.net/projects/p/pondus/team/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=2; plural=(n != 1)
 Aggiungi misurazione Aggiungi misurazione Intero periodo Si è verificato un errore durante l'importazione. Sembra che ci sia già un pondus in esecuzione su questo file. Si vuole veramente continuare e perdere tutte le modifiche dell'altro programma in esecuzione? Indice di Massa Corporea Esporta CSV Leggi dal file CSV: Salva il file CSV in: Importa CSV Impossibile trovare %s Personalizzato Dati da esportare: Il file dati è bloccato, continuare comunque? Data Data (YYYY-MM-DD) Elimina la riga selezionata Si desidera veramente rimuovere questa misurazione? Modifica misurazione Modifica la misurazione selezionata Usa la proiezione del peso Fine del periodo Errore: File non valido Errore: formato errato. Esportazione avvenuta con successo Importa i dati in: L'importazione non ha avuto successo Importazione avvenuta con successo Ultimi 6 mesi Ultimo mese Ultimo anno Matplotlib non è disponibile! Non si dispone del blocco sul file. Verrà effettuato un backup in %s Verificare che pygtk sia installato. Crea un grafico Crea un grafico del peso Crea un grafico del peso Preferenze Sistema di misura preferito: Esci Lettura del file %s Ricordare le dimensioni della finestra Rimuovere i dati? Salva il grafico Salva in un file di tipo: Salva in un file Salvataggio del grafico in %s Seleziona l'intervallo di tempo: Seleziona file Seleziona l'intervallo di tempo: Smussato Inizio del periodo I dati inseriti non sono nel formato corretto! L'esportazione è avvenuta con successo. Il percorso indicato non corrisponde a un file valido! L'importazione è avvenuta con successo. La data di inizio deve essere precedente alla data di fine periodo! La proiezione del peso può essere abilitata nelle preferenze Per disegnare l'Indice di Massa Corporea è necessario inserire la propria altezza nelle preferenze. Usa un calendario nella finestra Aggiungi misurazione Mostra un calendario invece che inserire manualmente le date Altezza dell'utente: Viene usato il file standard ~/.pondus/user_data.xml in sostituzione. Peso Misurazioni del peso Proiezione del peso Proiezione del peso Questa è una directory, non un file! cm ft anglosassone in kg lbs m metrico Matplotlib non è disponibile, la creazione di grafici è disabilitata peso.csv grafico_peso.png 