��    \      �     �      �     �     �     �  #   �  �        �     �     �  
   �     �     	  
   	     %	     7	  
   >	     I	     Y	     t	     y	     �	  *   �	     �	     �	     �	     
     
     (
     <
     N
     ^
     t
     �
  
   �
  	   �
     �
     �
     �
  3   �
       $        -     2     >     O     [     r     w     �     �     �  	   �     �     �     �     �     �       	   %     /  
   6  .   A     p  .   �     �  -   �  <     J   @     �     �     �     �  S   �     !  8   .     g     m     t     �     �  #   �     �     �     �     �     �     �     �     �  6   �  
         +  �  ;          7     U  @   g  #  �  5   �           #     *  !   @  2   b     �  "   �      �     �  $   	  G   .     v       .   �  P   �  '     :   E  0   �     �  /   �  +   �  +   '  +   S  2     )   �  #   �                 8  
   Y     d  �   k     �  L        Z     g  $   }     �  9   �  
   �     �  *        @     ]     i  ,   �     �  )   �  .   �     .  D   F     �     �     �  ?   �  2     c   I  0   �  M   �  h   ,  p   �       !   $     F  R   f  �   �  "   V  P   y     �     �     �     �       3   (     \     a     d     u     x     }     �     �  f   �  
               =       :       J   R   &              +                K       Y   >             @         [   !   '          5   Q       .       F   3          )      A   C       ,      ;       8   /   "   <   ?   %   4                      #   B          7                      I       X              $      L          9       N   O   Z   H   2                U       *   S   T   P   D   -   
   (                   6   G      E       V           1   M       \   	   W             0        Add Dataset Add dataset All Time An error occured during the import. Another instance of pondus seems to be editing the same datafile. Do you really want to continue and loose all the changes from the other instance? Append Notes to Datasets Body Mass Index Bodyfat CSV Export CSV File to read from: CSV File to save to: CSV Import Could not find %s Custom Data Left: Data to export: Datafile locked, continue? Date Date (YYYY-MM-DD) Delete selected dataset Do you really want to delete this dataset? Edit Dataset Edit selected dataset Enable Weight Planner End date Error: Not a valid File Error: Wrong Format Export successful Import data to: Import not successful Import successful Last 6 Months Last Month Last Year Matplotlib not available! Muscle None Not owning the file lock. Backing up the data to %s Note Please make sure pygtk is installed. Plot Plot Weight Plot weight data Preferences Preferred Unit System: Quit Reading file %s Remember Window Size Remove Data? Right: Save Plot Save as File Type: Save to File Saving plot to %s Select Date Range: Select File Select date range of plot Show Plan Smooth Start date The data entered is not in the correct format! The export was successful. The given path does not point to a valid file! The import was successful. The start date has to be before the end date! The weight planner can be enabled in the preferences dialog. To plot your BMI, you need to enter your height in the preferences dialog. Track Bodyfat Track Muscle Track Water Use Calendar in Add Dialog Use a calendar widget instead of a text entry to enter dates in the add/edit dialog User Height: Using the standard file ~/.pondus/user_data.xml instead. Water Weight Weight Measurements Weight Plan Weight Planner You passed a directory, not a file! cm ft imperial in kg lbs m metric python-matplotlib is not installed, plotting disabled! weight.csv weight_plot.png Project-Id-Version: pondus
Report-Msgid-Bugs-To: http://bitbucket.org/eike/pondus/issues
POT-Creation-Date: 2011-05-28 20:39+0200
PO-Revision-Date: 2011-05-31 13:24+0000
Last-Translator: Sl_Alex <alexslabchenko@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
 Добавить данные Добавить данные Всё время В процессе импорта возникла ошибка Кажется, другая версия pondus уже редактирует этот файл данных. Вы действительно хотите продолжить и потерять все изменения, сделанные в другой версии программы? Добавить примечания к данным Индекс массы тела Жир Экспорт в CSV CSV-файл для чтения: Имя CSV-файла для сохранения: Импорт CSV Невозможно найти %s Пользовательский Данных осталось: Данные для экспорта База данных заблокирована, продолжить? Дата Дата (ГГГГ-ММ-ДД) Удалить выбранные данные Вы действительно хотите удалить эти данные? Редактировать данные Редактировать выбранные данные Включить планировщик веса Конечная дата Ошибка: Неправильный файл Ошибка: неверный формат Успешно экспортировано Импортировать данные в: Импорт закончился неудачно Успешно импортировано Последние 6 месяцев Последний месяц Последний год Matplotlib недоступна! Мышцы Нет Не являемся владельцем файла блокировки. Выполняется копирование данных в %s Примечание Пожалуйста, убедитесь, что pygtk установлен. График График веса Вывести график веса Настройки Предпочитаемая система единиц: Выход Чтение файла %s Запоминать размер окна Удалить данные? Верно: Сохранить график Сохранить как файл типа: Сохранить в файл График сохраняется в %s Выберите диапазон данных Выбрать файл Выберите диапазон данных для графика Показать план Сглаженный Начальная дата Данные введены в неверном формате! Экспорт закончился успешно Предоставленный путь не указывает на правильный файл! Импорт завершился успешно Дата начала должна быть меньше даты конца! Планировщик веса должен быть включен в диалоге настроек. Чтобы построить ваш ИМТ, введите свой рост в диалоге настроек Отслеживать жир Отслеживать мышцы Отслеживать воду Использовать календарь в диалоге добавления Использовать календарь вместо текстового ввода в диалоге добавления/редактирования Рост пользователя: Используется стандартный файл ~/.pondus/user_data.xml Вода Вес Измерения веса План веса Планировщик веса Вы передали папку, а не файл! см ft дюймовая in кг lbs м метрическая python-matplotlib не установлен, построение графиков отключено! weight.csv weight_plot.png 