��    \      �     �      �     �     �     �  #   �  �        �     �     �  
   �     �     	  
   	     %	     7	  
   >	     I	     Y	     t	     y	     �	  *   �	     �	     �	     �	     
     
     (
     <
     N
     ^
     t
     �
  
   �
  	   �
     �
     �
     �
  3   �
       $        -     2     >     O     [     r     w     �     �     �  	   �     �     �     �     �     �       	   %     /  
   6  .   A     p  .   �     �  -   �  <     J   @     �     �     �     �  S   �     !  8   .     g     m     t     �     �  #   �     �     �     �     �     �     �     �     �  6   �  
         +  �  ;     �     �     �  "   �  �        �     �     �  
   �     �  !     
   )     4     D     L     _     x     �     �     �  (   �            !   .     P     ]     z     �     �     �     �     �     �     �          !     '  ;   3     o  !   v     �     �     �  	   �     �     �     �             	   /     9     J     f     z     �     �     �     �     �     �  .   �     &  9   <     v  >   �  B   �  S        b     �     �  4   �  g   �     G  B   \     �     �     �     �     �  #   �                         $     '     .     0  <   9  
   v     �     =       :       J   R   &              +                K       Y   >             @         [   !   '          5   Q       .       F   3          )      A   C       ,      ;       8   /   "   <   ?   %   4                      #   B          7                      I       X              $      L          9       N   O   Z   H   2                U       *   S   T   P   D   -   
   (                   6   G      E       V           1   M       \   	   W             0        Add Dataset Add dataset All Time An error occured during the import. Another instance of pondus seems to be editing the same datafile. Do you really want to continue and loose all the changes from the other instance? Append Notes to Datasets Body Mass Index Bodyfat CSV Export CSV File to read from: CSV File to save to: CSV Import Could not find %s Custom Data Left: Data to export: Datafile locked, continue? Date Date (YYYY-MM-DD) Delete selected dataset Do you really want to delete this dataset? Edit Dataset Edit selected dataset Enable Weight Planner End date Error: Not a valid File Error: Wrong Format Export successful Import data to: Import not successful Import successful Last 6 Months Last Month Last Year Matplotlib not available! Muscle None Not owning the file lock. Backing up the data to %s Note Please make sure pygtk is installed. Plot Plot Weight Plot weight data Preferences Preferred Unit System: Quit Reading file %s Remember Window Size Remove Data? Right: Save Plot Save as File Type: Save to File Saving plot to %s Select Date Range: Select File Select date range of plot Show Plan Smooth Start date The data entered is not in the correct format! The export was successful. The given path does not point to a valid file! The import was successful. The start date has to be before the end date! The weight planner can be enabled in the preferences dialog. To plot your BMI, you need to enter your height in the preferences dialog. Track Bodyfat Track Muscle Track Water Use Calendar in Add Dialog Use a calendar widget instead of a text entry to enter dates in the add/edit dialog User Height: Using the standard file ~/.pondus/user_data.xml instead. Water Weight Weight Measurements Weight Plan Weight Planner You passed a directory, not a file! cm ft imperial in kg lbs m metric python-matplotlib is not installed, plotting disabled! weight.csv weight_plot.png Project-Id-Version: pondus
Report-Msgid-Bugs-To: http://bitbucket.org/eike/pondus/issues
POT-Creation-Date: 2011-05-28 20:39+0200
PO-Revision-Date: 2011-06-01 09:04+0000
Last-Translator: erauti <erauti@suomi24.fi>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1)
 Lisää tietoja Lisää tietoja Kaikki Tapahtui virhe tuonnin yhteydessä Ponduksen toinen ilmentymä on muokkaamassa samaa datatiedostoa. Haluatko jatkaa ja mahdollisesti menettää toisen ilmentymän tekemät muutokset? Lisää huomioita tietueisiin Body Mass Index Kehon rasva CSV vienti CVS tiedosto, josta luetaan: CVS tiedosto, johon tallennetaan: CVS tuonti Ei löytynyt %s Valittu Tiedot vasemmalle: Tiedot, jotka viedään: Datatiedosto lukittu, jatkatko? Päivämäärä Päivämäärä (YYYY-MM-DD) Poista valitut tiedot Haluatko varmasti poistaa nämä tiedot? Muokkaa tietoja Muokkaa valittuja tietoja Ota käyttöön painonsuunnittelu Lopetus pvm. Virhe: Virheellinen tiedosto Virhe: Virheellinen muoto Vienti onnistui Tuo tiedot: Tuonti ei onnistunut Tuonti onnistui Viimeiset 6 kuukautta Viime kuukausi Viime vuosi Matplotlib ei asennettuna! Lihas Ei mitään Ei oikeutta tiedoston lukitsemiseen. Tallennetaan tiedot %s Huomio Varmista että pygtk on asennettu Piirrä Piirrä painokäyrä Piirrä painotiedot Asetukset Oletus yksikköjärjestelmä: Lopeta Luetaan tiedosto %s Muista ikkunan koko Poista tiedot? Oikealle: Tallenna piirros Tallenna tiedostotyyppinä: Tallenna tiedostoon Talletetaan piirros %s Valitse ajanjakso: Valitse tiedosto Valitse piirron ajanjakso Näytä suunnitelma Sileä Aloitus pvm. Syötetyt tiedot eivät ole oikein muotoiltuja Vienti oli onnistunut Annettu hakemistopolku ei sisällä soveltuvaa tiedostoa! Tuonti oli onnistunut Aloituspäivämäärän tulee olla ennen päättymispäivää! Painonsuunnittelu voidaan ottaa käyttöön asetukset lomakkeella. Piirtääksesi BMI käyrän, tulee sinun antaa pituutesi asetustietojen lomakkeella Seuraa kehon rasvapitoisuutta Seuraa lihasmassaa Seuraa vesipitoisuutta Käytä kalenteria päivämäärän syöttämisessä Käytä kalenteria päivämäärien syöttämiseen tekstinsyötön sijaan lisää/muokkaa -lomakkeella  Käyttäjän pituus: Käytetään tilalla standardia tiedostoa ~/.pondus/user_data.xml. Vesi Paino Painon mittaukset Painonsuunnittelu Painonsuunnittelu Annoit hakemiston tiedoston sijaan! cm jalkaa englantilainen tuumaa kg paunaa m metrinen python-matplotlib ei ole asennettuna, piirto ei käytössä! weight.csv weight_plot.png 