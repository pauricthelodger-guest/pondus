��    \      �     �      �     �     �     �  #   �  �        �     �     �  
   �     �     	  
   	     %	     7	  
   >	     I	     Y	     t	     y	     �	  *   �	     �	     �	     �	     
     
     (
     <
     N
     ^
     t
     �
  
   �
  	   �
     �
     �
     �
  3   �
       $        -     2     >     O     [     r     w     �     �     �  	   �     �     �     �     �     �       	   %     /  
   6  .   A     p  .   �     �  -   �  <     J   @     �     �     �     �  S   �     !  8   .     g     m     t     �     �  #   �     �     �     �     �     �     �     �     �  6   �  
         +  �  ;      �           3  F   M    �  :   �  $   �               7  %   W     }  '   �     �     �  !   �  P        c     l  5   �  M   �  *     9   9  >   s     �  )   �  /   �     "  "   B  )   e     �     �     �     �  <        D     ^  k   e     �  <   �       "   ,  6   O     �  3   �  
   �     �  9   �  1   0     b  *   r  1   �  "   �  -   �           >  5   W  .   �     �     �  K   �  '   3  D   [  %   �  V   �  y     �   �  5   I  1     /   �  8   �  �     -   �  g         h  
   �  (   �  &   �  &   �  3         6      9      <      M      P      S      W      Y   |   j   
   �      �      =       :       J   R   &              +                K       Y   >             @         [   !   '          5   Q       .       F   3          )      A   C       ,      ;       8   /   "   <   ?   %   4                      #   B          7                      I       X              $      L          9       N   O   Z   H   2                U       *   S   T   P   D   -   
   (                   6   G      E       V           1   M       \   	   W             0        Add Dataset Add dataset All Time An error occured during the import. Another instance of pondus seems to be editing the same datafile. Do you really want to continue and loose all the changes from the other instance? Append Notes to Datasets Body Mass Index Bodyfat CSV Export CSV File to read from: CSV File to save to: CSV Import Could not find %s Custom Data Left: Data to export: Datafile locked, continue? Date Date (YYYY-MM-DD) Delete selected dataset Do you really want to delete this dataset? Edit Dataset Edit selected dataset Enable Weight Planner End date Error: Not a valid File Error: Wrong Format Export successful Import data to: Import not successful Import successful Last 6 Months Last Month Last Year Matplotlib not available! Muscle None Not owning the file lock. Backing up the data to %s Note Please make sure pygtk is installed. Plot Plot Weight Plot weight data Preferences Preferred Unit System: Quit Reading file %s Remember Window Size Remove Data? Right: Save Plot Save as File Type: Save to File Saving plot to %s Select Date Range: Select File Select date range of plot Show Plan Smooth Start date The data entered is not in the correct format! The export was successful. The given path does not point to a valid file! The import was successful. The start date has to be before the end date! The weight planner can be enabled in the preferences dialog. To plot your BMI, you need to enter your height in the preferences dialog. Track Bodyfat Track Muscle Track Water Use Calendar in Add Dialog Use a calendar widget instead of a text entry to enter dates in the add/edit dialog User Height: Using the standard file ~/.pondus/user_data.xml instead. Water Weight Weight Measurements Weight Plan Weight Planner You passed a directory, not a file! cm ft imperial in kg lbs m metric python-matplotlib is not installed, plotting disabled! weight.csv weight_plot.png Project-Id-Version: pondus
Report-Msgid-Bugs-To: http://bitbucket.org/eike/pondus/issues
POT-Creation-Date: 2011-05-28 20:39+0200
PO-Revision-Date: 2011-06-02 10:29+0000
Last-Translator: turin <yasen@lindeas.com>
Language-Team: Bulgarian (http://www.transifex.net/projects/p/pondus/team/bg/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: bg
Plural-Forms: nplurals=2; plural=(n != 1)
 Добавяне на данни Добавяне на данни Целият период Възникна грешка по време на внасянето. Изглежда друго копие на програмата редактира същия файл с данни. Наистина ли искате да се продължи и да се загубят всички промени от другото копие? Добавяне на бележки към данните Индекс телесна маса Телесна мазнина Изнасяне в CSV Файл CSV за четене: Файл CSV за запазване: Внасяне от CSV Не може да се открие %s Ръчно зададен Данни отляво: Данни за изнасяне: Файлът с данни е заключен. Да се продължи ли? Дата Дата (ГГГГ-ММ-ДД) Изтриване на избраните данни Наистина ли искате да изтриете тези данни? Редактиране на данните Редактиране на избраните данни Включване на планиране на теглото Крайна дата Грешка: Невалиден файл Грешка: неправилен формат Успешно изнасяне Внасяне на данни в: Внасянето е неуспешно. Успешно внасяне Последни 6 месеца Последен месец Последна година Библиотеката mathplotlib не е налична! Мускулна маса Без Файлът е заключен. Създава се резервно копие на данните в %s Бележка Проверете дали pygtk е инсталирано. Графика Графика на теглото Графика на данните за теглото Настройки Предпочитана мерна система: Изход Четене на файл %s Запомняне размера на прозореца Да се премахнат ли данните? Отдясно: Запазване на графиката Запазване като файл от вид: Запазване във файл Запазване на графиката %s Избор на период: Избор на файл Изберете период за показване Показване на планирането Усреднено Начална дата Въведените данни не са в правилен формат! Изнасянето е успешно. Даденият път не съдържа валиден файл! Внасянето е успешно. Началната дата трябва да е преди крайната дата! Планирането на теглото може да се включи от прозореца с настройки. За да се изчертае вашия ИТМ (индекс телесна маса), трябва да въведете в настройките височината си. Следене на телесната мазнина Следене на мускулната маса Следене на телесната вода Календар при добавяне на данни Използване на календар вместо текстово поле за въвеждане на дати при добавяне или редакция на данни Височина на потребителя: Използване вместо това на стандартния файл ~/.pondus/user_data.xml Телесна вода Тегло Измервания на теглото Планиране на теглото Планиране на теглото Това е директория, а не файл! cm ft имперска in kg lbs m метрична python-matplotlib не е инсталирано, затова чертането на графики е изключено! weight.csv weight_plot.png 